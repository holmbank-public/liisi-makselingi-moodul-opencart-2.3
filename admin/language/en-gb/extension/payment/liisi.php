<?php
/**
 * Copyright (c) 2012-2015, liisi B.V.
 * All rights reserved. 
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met: 
 * 
 * - Redistributions of source code must retain the above copyright notice, 
 *    this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright 
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
 * DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGE. 
 *
 * @package     liisi
 * @license     Berkeley Software Distribution License (BSD-License 2) http://www.opensource.org/licenses/bsd-license.php
 * @author      liisi B.V. <info@liisi.com>
 * @copyright   liisi B.V.
 * @link        https://www.liisi.com
 */

// Heading
$_['heading_title']           = "Liisi credit";
$_['title_global_options']    = "Options for all liisi modules";
$_['title_payment_status']    = "Payment statuses";
$_['title_mod_about']         = "About this module";
$_['footer_text']             = "Payment services";

// Module names
$_['name_liisi_banktransfer'] = "Bank transfer";
$_['name_liisi_belfius']      = "Belfius Direct Net";
$_['name_liisi_bitcoin']      = "Bitcoin";
$_['name_liisi_creditcard']   = "Creditcard";
$_['name_liisi_ideal']        = "iDEAL";
$_['name_liisi_mistercash']   = "Bancontact/MisterCash";
$_['name_liisi_paypal']       = "PayPal";
$_['name_liisi_paysafecard']  = "paysafecard";
$_['name_liisi_sofort']       = "SOFORT Banking";

// Text
$_['text_edit']                    = "Edit";
$_['text_payment']                 = "Payment";
$_['text_success']                 = "Success: You have successfully modified your liisi settings!";
$_['text_missing_api_key']         = "Please fill out your API key below.";
$_['text_activate_payment_method'] = 'Enable this payment method in your <a href="https://www.liisi.com/beheer/account/profielen/" target="_blank">liisi dashboard</a>.';
$_['text_no_status_id']            = "- Do not update the order status (not recommended) -";

// Entry
$_['entry_payment_method']           = "Payment method";
$_['entry_field_server']           = "Server";
$_['entry_field_snd']           = "SND ID";
$_['entry_field_public']           = "Public key";
$_['entry_field_private']           = "Private key";
$_['entry_field_private_password']           = "Private key password";
$_['entry_field_payment_confirm']           = "Payment confirmation text";
$_['entry_field_status']           = "Status";

// Info
$_['entry_module']            = "Module";
$_['entry_mod_status']        = "Module status";
$_['entry_comm_status']       = "Communication status";
$_['entry_support']           = "Support";

// Error
$_['error_permission']        = "Warning: You don't have permission to modify the liisi payment methods.";

// Status
$_['entry_pending_status']    = "Payment created status";
$_['entry_failed_status']     = "Payment failed status";
$_['entry_canceled_status']   = "Payment canceled status";
$_['entry_expired_status']    = "Payment expired status";
$_['entry_processing_status'] = "Payment successful status";
