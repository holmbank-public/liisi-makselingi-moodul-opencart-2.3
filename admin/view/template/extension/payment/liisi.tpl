<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-bank-transfer" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-bank-transfer" class="form-horizontal">
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="liisi_status"><?php echo $entry_field_status; ?></label>
            <div class="col-sm-10">
              <select name="liisi_status" id="liisi_status" class="form-control">
                <option value="0" <?php echo ($liisi_status == 0 ? 'selected="selected"' : ''); ?>>Disabled</option>
                <option value="1" <?php echo ($liisi_status == 1 ? 'selected="selected"' : ''); ?>>Enabled</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="liisi_server"><?php echo $entry_field_server; ?></label>
            <div class="col-sm-10">
              <select name="liisi_server" id="liisi_server" class="form-control">
                <option value="0" <?php echo ($liisi_server == 0 ? 'selected="selected"' : ''); ?>>Test</option>
                <option value="1" <?php echo ($liisi_server == 1 ? 'selected="selected"' : ''); ?>>Live</option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="liisi_snd_id"><?php echo $entry_field_snd; ?></label>
            <div class="col-sm-10">
              <input type="text" name="liisi_snd_id" id="liisi_snd_id" value="<?php echo $liisi_snd_id; ?>" placeholder="<?php echo $entry_field_snd; ?>" class="form-control" />
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="liisi_public"><?php echo $entry_field_public; ?></label>
            <div class="col-sm-10">
              <textarea name="liisi_public" id="liisi_public" class="form-control"><?php echo $liisi_public; ?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="liisi_private"><?php echo $entry_field_private; ?></label>
            <div class="col-sm-10">
              <textarea name="liisi_private" id="liisi_private" class="form-control"><?php echo $liisi_private; ?></textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="liisi_private_password"><?php echo $entry_field_private_password; ?></label>
            <div class="col-sm-10">
              <input type="password" name="liisi_private_password" id="liisi_private_password" value="<?php echo $liisi_private_password; ?>" placeholder="<?php echo $liisi_private_password; ?>" class="form-control" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="liisi_payment_confirm"><?php echo $entry_field_payment_confirm; ?></label>
            <div class="col-sm-10">
              <input type="text" name="liisi_payment_confirm" id="liisi_payment_confirm" value="<?php echo $liisi_payment_confirm; ?>" placeholder="<?php echo $entry_field_payment_confirm; ?>" class="form-control" />
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>