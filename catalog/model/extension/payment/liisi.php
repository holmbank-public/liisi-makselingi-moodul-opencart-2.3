<?php
class ModelExtensionPaymentLiisi extends Model {
  public function getMethod($address, $total) {
    $this->load->language('extension/payment/liisi');
  
    $method_data = array(
      'code'     => 'liisi',
      'title'    => $this->language->get('text_title'),
      'terms'      => '',
      'sort_order' => $this->config->get('liisi_sort_order')
    );
  
    return $method_data;
  }
}
?>
