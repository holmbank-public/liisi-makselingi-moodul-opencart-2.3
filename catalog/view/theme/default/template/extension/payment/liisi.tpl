<form action="<?php echo $action; ?>" method="post">
  
  <?php
  foreach ((array)$formFields as $key=>$val) {
    echo '<input type="hidden" name="'.$key.'" value="'.$val.'" />';
  }

  ?>
  
  <div class="buttons">
    <div class="right">
      <input type="submit" value="<?php echo $button_confirm; ?>" class="button" />
    </div>
  </div>
</form>